import torch

def TransToRp(v):
    return v[:, :,0: 3, 0:3], v[:, :,0:3, 3]


def TensorToSo3(v):
    batch_size = v.shape[0]
    res = torch.zeros(batch_size, 3, 3, dtype=torch.float64)

    res[:, 1, 2] = -v[:, 0]
    res[:, 2, 1] = v[:, 0]
    res[:, 2, 0] = -v[:, 1]
    res[:, 0, 2] = v[:, 1]
    res[:, 0, 1] = -v[:, 2]
    res[:, 1, 0] = v[:, 2]

    return res


def TensorToSe3(sTimesTheta):
    N = sTimesTheta.shape[0]
    numberOfLinks = sTimesTheta.shape[1]

    # prva tri stupca iz matrice sTimesTheta
    sTimesThetaFirstThreeColumns = sTimesTheta[:, :, :3]

    v = sTimesThetaFirstThreeColumns.reshape(-1, 3)
    so3Matrices = TensorToSo3(v)
    # sve so3 matrice
    so3Matrices = so3Matrices.reshape(N, numberOfLinks, 3, 3)

    return so3Matrices

def TensorToExp6(sTimesTheta, thetaList):
    N = sTimesTheta.shape[0]
    numberOfLinks = sTimesTheta.shape[1]
    NTimesNumLinks = N * numberOfLinks

    # sve se3 matrice
    se3Matrices = TensorToSe3(sTimesTheta)
    thetaList = thetaList.reshape(NTimesNumLinks, thetaList.shape[2])
    # dobivamo (N * num_links) * 1 * 1 tensor
    thetaList.unsqueeze_(2)
    se3Matrices = se3Matrices.reshape(NTimesNumLinks, 3, 3)

    # jedinicna matrica (1 x 3 x 3)
    unitMatrix = torch.eye(3).unsqueeze(0)
    # Predstavlja 1 u jednazbi => za oduzimanje 1 - cos(theta)
    ones = torch.ones(NTimesNumLinks, 1, 1)
    # sve 0 pretvaramo u malu vrijednost (da mozemo dijeliti)
    thetaList[thetaList == 0] = 1e-10

    sTimesThetaFirstThreeColumns = sTimesTheta[:, :, :3].reshape(-1, 3)
    axisAngles = torch.norm(sTimesThetaFirstThreeColumns, dim=1).reshape(NTimesNumLinks, 1, 1)
    axisAngles[axisAngles == 0] = 1e-6

    omegaMat = se3Matrices / axisAngles

    R = unitMatrix \
        + (torch.sin(axisAngles) * omegaMat) \
        + (ones - torch.cos(axisAngles)) * torch.matmul(omegaMat, omegaMat)

    V = unitMatrix * axisAngles \
        + (ones - torch.cos(axisAngles)) * omegaMat \
        + (axisAngles - torch.sin(axisAngles)) * torch.matmul(omegaMat, omegaMat)

    sTimesThetaLastThreeColumns = sTimesTheta[:, :, 3:].reshape(NTimesNumLinks, 3, 1)
    VTimesRo = torch.matmul(V, sTimesThetaLastThreeColumns / axisAngles)
    R = torch.cat((R, VTimesRo), 2)

    finalTensor = torch.zeros(NTimesNumLinks, 4, 4)
    finalTensor = finalTensor[:, 3:]
    finalTensor[:, :, 3] = 1
    finalTensor = torch.cat((R, finalTensor), 1)
    return finalTensor.reshape(N, numberOfLinks, 4, 4)



# result => tenzor u koji spremamo rezultate
def MulMatricesInTensor(input, result):
    # split po prvoj dimenziji (N clanova velicine (num_links + 1) x 4 x 4)
    tuples = torch.unbind(input, 0)

    for i in range(len(tuples)):
        exp6Matrices = torch.unbind(tuples[i])
        result[i] = torch.linalg.multi_dot(exp6Matrices)

    return result


def AdjointTensor(T):
    R, p = TransToRp(T)
    # finalna N x numLinks - 1 x 6 x 6 matrica
    result = torch.cat((R, torch.zeros(R.shape[0], R.shape[1], 3, 3)), dim=3)
    a = p.reshape(-1, 3)
    so3Matrices = TensorToSo3(a)
    b = torch.bmm(so3Matrices, R.reshape(-1, 3, 3))
    b = torch.cat((b.reshape(R.shape[0], R.shape[1], 3, 3), R), dim=3)

    return torch.cat((result,b), dim=2)
