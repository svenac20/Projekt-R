import threading

import torch
from torch.linalg import multi_dot
from MatrixFunctions import *
import time


class ForwardKinematicsThread():
    def __init__(self, M, S, thetaList):
        self.M = M
        self.S = S
        self.thetaList = thetaList
        self.N = self.S.shape[0]
        self.numberOfLinks = self.S.shape[1]

    def calculate(self):
        # dodajemo dimenziju na tenzor da mozemo mnoziti matrice
        self.thetaList.unsqueeze_(1)
        self.thetaList.transpose_(1, 2)
        # mnozenje sa svim konfiguracijama (dobijamo N x num_joints x 6 tensor)
        sTimesTheta = self.S * self.thetaList
        exp6Tensor = TensorToExp6(sTimesTheta, self.thetaList)
        numLinksWithM = self.numberOfLinks + 1

        # +1 da dodamo M matricu kao zadnji clan
        matrixMul = torch.zeros(self.N, numLinksWithM, 4, 4)
        matrixMul[:, :self.numberOfLinks] = exp6Tensor[:]
        matrixMul[:, -1] = self.M[:]
        resultTensor = torch.zeros(self.N, 4, 4, dtype=torch.float64)

        return MulMatricesInTensor(matrixMul, resultTensor)

        # print(time.time_ns())
        # startTensor = torch.zeros(self.N, 4, 4, dtype=torch.float64)
        # a_split = list(torch.split(exp6Tensor, 1, dim=1))
        # for i in range(len(a_split)):
        #     a_split.insert(2 * i + 1, [..., i, i + 1])
        # out_3 = torch.einsum(*a_split)
        # torch.bmm(out_3.squeeze(1), self.M)
        # print(time.time_ns())
        #
        # program_starts = time.time_ns()
        # for i in range(0, exp6Tensor.shape[1], 1):
        #     startTensor = torch.bmm(startTensor, exp6Tensor[:, i])
        # torch.bmm(startTensor, self.M)
        # print(time.time_ns() - program_starts)


