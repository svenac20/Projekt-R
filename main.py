import math

import numpy as np

from ForwardKinematicsThread import ForwardKinematicsThread
from JacobianSpace import JacobianSpace
import matplotlib.pyplot as plt
import torch

import warnings

warnings.filterwarnings("ignore", category=np.VisibleDeprecationWarning)

# Number of configurations
N = 3
# Number of links in robot
numberOfLinks = 6
# Length of each link
L = [2, 2, 2, 2, 2, 2]
# Length of end effector
endEffectorLength = 0.5
# Desired configuration for visualisation [0, numberOfLinks - 1]
INDEX = 0

configurations = torch.as_tensor([[0, -np.pi / 2, 0, 0, np.pi / 2, 0],
                                  [0, -np.pi / 2, 0, 0, np.pi / 2, 0],
                                  [0, -np.pi / 2, 0, 0, np.pi / 2, 0]])

# Matrix M (parameters of robot)
M = torch.as_tensor([
    [[-1, 0, 0, 817],
     [0, 0, 1, 191],
     [0, 1, 0, -6],
     [0, 0, 0, 1]],
    [[-1, 0, 0, 817],
     [0, 0, 1, 191],
     [0, 1, 0, -6],
     [0, 0, 0, 1]],
    [[-1, 0, 0, 817],
     [0, 0, 1, 191],
     [0, 1, 0, -6],
     [0, 0, 0, 1]],
], dtype=float)

# Enter data in table like format
S = torch.as_tensor(
    np.array(
        [
            [
                [0, 0, 1, 0, 0, 0],
                [0, 1, 0, -89, 0, 0],
                [0, 1, 0, -89, 0, 425],
                [0, 1, 0, -89, 0, 817],
                [0, 0, -1, -109, 817, 0],
                [0, 1, 0, 6, 0, 817]
            ],
            [
                [0, 0, 1, 0, 0, 0],
                [0, 1, 0, -89, 0, 0],
                [0, 1, 0, -89, 0, 425],
                [0, 1, 0, -89, 0, 817],
                [0, 0, -1, -109, 817, 0],
                [0, 1, 0, 6, 0, 817]
            ],
            [
                [0, 0, 1, 0, 0, 0],
                [0, 1, 0, -89, 0, 0],
                [0, 1, 0, -89, 0, 425],
                [0, 1, 0, -89, 0, 817],
                [0, 0, -1, -109, 817, 0],
                [0, 1, 0, 6, 0, 817]
            ]
        ]), dtype=float)

Slist = torch.as_tensor(
    np.array(
        [
            [
                [0, 0, 1, 0, 0.2, 0.2],
                [1, 0, 0, 2, 0, 3],
                [0, 1, 0, 0, 2, 1],
                [1, 0, 0, 0.2, 0.3, 0.4]
            ],
            [
                [0, 0, 1, 0, 0.2, 0.2],
                [1, 0, 0, 2, 0, 3],
                [0, 1, 0, 0, 2, 1],
                [1, 0, 0, 0.2, 0.3, 0.4]
            ],
            [
                [0, 0, 1, 0, 0.2, 0.2],
                [1, 0, 0, 2, 0, 3],
                [0, 1, 0, 0, 2, 1],
                [1, 0, 0, 0.2, 0.3, 0.4]
            ],
        ]))

thetalist = torch.as_tensor([[0.2, 1.1, 0.1, 1.2], [0.2, 1.1, 0.1, 1.2], [0.2, 1.1, 0.1, 1.2]])

if __name__ == '__main__':
    # tenzor za racunanje
    fk = ForwardKinematicsThread(M, S, configurations)
    print(fk.calculate())

    js = JacobianSpace(Slist, thetalist)
    print(js.calculate())

