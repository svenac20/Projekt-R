import torch
from MatrixFunctions import *


class JacobianSpace():
    def __init__(self, S, thetaList):
        self.S = S
        self.thetaList = thetaList
        self.N = self.S.shape[0]
        self.numberOfLinks = self.S.shape[1]

    def calculate(self):
        self.thetaList.unsqueeze_(1)
        self.thetaList.transpose_(1, 2)

        sTimesTheta = self.S[:, :3] * self.thetaList[:, :3]
        # N x num_links - 1 x 4 x 4
        exp6Tensor = TensorToExp6(sTimesTheta, self.thetaList[:, :3])
        T = torch.eye(4, dtype=torch.float64)
        # tenzor za spremanje rezultata mnozenja matrica
        tensorT = torch.zeros(self.N, self.numberOfLinks, 4, 4, dtype=torch.float64)
        # na pocetni index stavljamo jedinicnu matricu
        tensorT[:, 0] = T

        for j in range(0, exp6Tensor.shape[1], 1):
            a = exp6Tensor[:, j]
            T = tensorT[:, j]
            tensorT[:, j + 1] = torch.bmm(T, a)

        # remove first 4x4 matrix
        tensorT = tensorT[:, 1:]
        a = AdjointTensor(tensorT).reshape(-1, 6, 6)
        S = self.S[:, 1:].reshape(-1, 6, 1)

        aTimesS = torch.bmm(a, S).reshape(self.N, self.numberOfLinks - 1, 6).transpose(1, 2)
        b = self.S[:, 0].unsqueeze(-1)
        return torch.cat((b, aTimesS), dim=2)
